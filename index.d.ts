export interface TapPayError extends Error {
  message: string, // 錯誤訊息
  code?: string, // 錯誤代碼
}

declare function getSDKVersion(): Promise<string>;

declare function createDirectPayToken(
  cardNumber: string,
  dueMonth: string,
  dueYear: string,
  ccv: string,
  geoLocation: string,
): Promise<string>;

declare function setup(
  appID: number,
  appKey: string,
  env: 'production' | 'sandbox',
): Promise<void>;
