
package com.kitchbot.module;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.app.Application;

import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import javax.annotation.Nullable;

import tech.cherri.tpdirect.api.TPDCard;
import tech.cherri.tpdirect.api.TPDMerchant;
import tech.cherri.tpdirect.api.TPDServerType;
import tech.cherri.tpdirect.api.TPDSetup;
import tech.cherri.tpdirect.callback.TPDGetPrimeFailureCallback;
import tech.cherri.tpdirect.callback.TPDCardGetPrimeSuccessCallback;
import tech.cherri.tpdirect.callback.dto.TPDCardInfoDto;
import tech.cherri.tpdirect.callback.dto.TPDMerchantReferenceInfoDto;

public class TapPay extends ReactContextBaseJavaModule {
  private final ReactApplicationContext reactContext;

  // TODO: check these are necessary or not.
  private static final int REQUEST_READ_PHONE_STATE = 101;
  private boolean isReadyPay = false;
  private TPDCard.CardType[] allowedNetworks = new TPDCard.CardType[] {
    TPDCard.CardType.Visa,
    TPDCard.CardType.MasterCard
  };

  public TapPay(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "TapPay";
  }

  @ReactMethod
  public void getSDKVersion(Promise promise) {
    promise.resolve(TPDSetup.getVersion());
  }

  @ReactMethod
  public void setup(int appID, String appKey, String dev, Promise promise) {
    Application applicationContext = (Application) reactContext.getApplicationContext();
    if(dev.equals("production")) {
      TPDSetup.initInstance(applicationContext, appID, appKey, TPDServerType.Production);
    }else{
      TPDSetup.initInstance(applicationContext, appID, appKey, TPDServerType.Sandbox);
    }

    promise.resolve("true");
  }

  @ReactMethod
  public void createDirectPayToken(
    String cardNumber,
    String dueMonth,
    String dueYear,
    String CCV,
    String geoLocation,
    final Promise promise
  ) {
    TPDCard card = new TPDCard(
      reactContext.getApplicationContext(),
      new StringBuffer(cardNumber),
      new StringBuffer(dueMonth),
      new StringBuffer(dueYear),
      new StringBuffer(CCV)
    );

    card.onSuccessCallback(new TPDCardGetPrimeSuccessCallback() {
      public void onSuccess(String prime, TPDCardInfoDto tpdCardInfo, String cardIdentifier, TPDMerchantReferenceInfoDto merchantReferenceInfo) {
        promise.resolve(prime);
      }
    }).onFailureCallback(new TPDGetPrimeFailureCallback() {
      public void onFailure(int status, String reportMsg) {
        promise.reject(String.valueOf(status),reportMsg);
      }
    });

    card.createToken(geoLocation);
  }
}
