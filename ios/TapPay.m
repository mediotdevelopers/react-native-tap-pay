#import "TapPay.h"
// #import <AdSupport/ASIdentifierManager.h>
#import <PassKit/PassKit.h>

@implementation TapPay
{
    bool hasListeners;
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()


- (NSArray<NSString *> *)supportedEvents
{
  return @[@"ApplePayStart",@"ApplePayFailed",@"ApplePaySuccessed",@"ApplePaySuccess",@"ApplePayCancel",@"ApplePayFinish",@"ApplePayDidSelectShipping"];
}


- (void)startObserving {
    hasListeners = YES;

}

// Will be called when this module's last listener is removed, or on dealloc.
- (void)stopObserving {
    hasListeners = NO;
}

RCT_EXPORT_METHOD(getSDKVersion:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  resolve([TPDSetup version]);
}

RCT_EXPORT_METHOD(setup:(int)appID
                  WithAPPKey:(NSString * _Nonnull)appKey
                  WithEnv:(NSString * _Nonnull)env
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  if([env isEqualToString:@"production"]) {
    [TPDSetup setWithAppId:appID withAppKey:appKey withServerType:TPDServer_Production];
  } else {
    [TPDSetup setWithAppId:appID withAppKey:appKey withServerType:TPDServer_SandBox];
  }

  resolve(@"true");
}


RCT_EXPORT_METHOD(createDirectPayToken:(NSString *_Nonnull)cardNumber
                  withDueMonth:(NSString * _Nonnull)dueMonth
                  withDueYear:(NSString * _Nonnull)dueYear
                  withCCV:(NSString * _Nonnull)CCV
                  withLocation:(NSString * _Nonnull)geoLocation
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  TPDCard *tpdCard = [TPDCard setWithCardNumber:cardNumber
                                    withDueMonth:dueMonth
                                    withDueYear:dueYear
                                        withCCV:CCV];
  [[[tpdCard onSuccessCallback:^(NSString * _Nullable prime, TPDCardInfo * _Nullable cardInfo, NSString * cardIdentifier, NSDictionary* merchantReferenceInfo) {
    resolve(prime);
  }] onFailureCallback:^(NSInteger status, NSString * _Nonnull message) {
    NSError *error = [NSError errorWithDomain:@"tappay:directpay" code:status userInfo:nil];
    NSString* code = [@(status) stringValue];

    reject(code,message,error);
  }] createTokenWithGeoLocation:geoLocation];
}
@end
