
#if __has_include(<React/RCTBridgeModule.h>)
  #import <React/RCTBridgeModule.h>
  #import <React/RCTEventEmitter.h>
#elif __has_include("React/RCTBridgeModule.h")
  #import "React/RCTBridgeModule.h"
  #import "React/RCTEventEmitter.h"
#else
  #import "RCTBridgeModule.h"
  #import "RCTEventEmitter.h"
#endif

#import <TPDirect/TPDirect.h>
#import <React/RCTConvert.h>

@interface TapPay : RCTEventEmitter

@end
